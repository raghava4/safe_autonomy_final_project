Instructions to run:

1. Make sure to comment/delete this line from the bash file: $ source /opt/ros/kinetic/setup.bash

2. Next start up the image classifier $ python OpenPoseImage.py test (this will not run without the previous step completed)

3. Source for ros $ source /opt/ros/kinetic/setup.bash

4. Start ros $ roscore

5. Start the turtlesim $ rosrun turtlesim turtlesim_node

6. Run the publisher $ rosrun beginner_tutorials talker.py

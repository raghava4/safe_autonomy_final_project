#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 12:01:09 2018

@author: keyur-r
"""

# Image Preprocessing for train, test and validation sets

import os
import random
import glob


def prepare_test_data(n):
    base_path = "poses"
    f1 = random.sample(glob.glob(os.path.join(base_path, "test/right") + "/*"), n)
    f2 = random.sample(glob.glob(os.path.join(base_path, "test/left") + "/*"), n)
    f3 = random.sample(glob.glob(os.path.join(base_path, "test/stop") + "/*"), n)
	f4 = random.sample(glob.glob(os.path.join(base_path, "test/go") + "/*"), n)
    for c in f1:
        os.remove(c)
    for s in f2:
        os.remove(s)
    for t in f3:
        os.remove(t)
	for z in f4:
		os.remove(z)

def prepare_validation_data(n):
    base_path = "poses"
    f1 = random.sample(glob.glob(os.path.join(base_path, "validation/right") + "/*"), n)
    f2 = random.sample(glob.glob(os.path.join(base_path, "validation/left") + "/*"), n)
    f3 = random.sample(glob.glob(os.path.join(base_path, "validation/stop") + "/*"), n)
	f4 = random.sample(glob.glob(os.path.join(base_path, "validation/go") + "/*"), n)
    for c in f1:
        os.remove(c)
    for s in f2:
        os.remove(s)
    for t in f3:
        os.remove(t)
	for z in f4:
		os.remove(z)

# n = number  of samples to remove from each categorical folder
prepare_test_data(70)
prepare_validation_data(40)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
import cv2
import argparse

from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import confusion_matrix, accuracy_score

from myutil import probas_to_classes


# Loading and compiling presaved trained CNN
model = load_model('pose_classification.h5')


label = {0: "Go", 1: "Left", 2: "Right", 3:"Stop"}

def predict_one(file_name):

    img = cv2.imread(file_name)
    img = cv2.resize(img, (28, 28))
    img = np.reshape(img, [1, 28, 28, 3])
    classes = model.predict_classes(img)[0]
    category = label[classes]
    f = open("/home/raghava/safe_autonomy_final_project/classifier/classification.txt", "w")
    f.write(category)
    f.close()
    print("\n{1} is {0}".format(category, file_name))
    # return category


def predict_dataset(input_dir):
    test_datagen = ImageDataGenerator(rescale=1. / 255)
    test_generator = test_datagen.flow_from_directory("poses/test",
                                                      target_size=(28, 28),
                                                      color_mode="rgb",
                                                      shuffle=False,
                                                      class_mode='categorical',
                                                      batch_size=1)
    filenames = test_generator.filenames
    nb_samples = len(filenames)
    predict = model.predict_generator(test_generator, steps=nb_samples)
    return predict, test_generator


def main():
    predict_one("/home/raghava/safe_autonomy_final_project/classifier/test/Output-Keypoints.jpg")

if __name__ == '__main__':
    main()
